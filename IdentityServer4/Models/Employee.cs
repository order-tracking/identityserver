﻿using Newtonsoft.Json;

namespace oTrackIdentityServer.Models {

    public class Employee {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("companyId")]
        public int CompanyId { get; set; }
        
        [JsonProperty("companyName")]
        public string CompanyName { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }
        
        [JsonProperty("prefixedPhoneNumber")]
        public string PrefixedPhoneNumber { get; set; }

        [JsonProperty("role")]
        public string Role { get; set; }
    }

}
