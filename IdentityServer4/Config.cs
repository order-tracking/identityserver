﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;

namespace oTrackIdentityServer
{
    public class Config
    {
        // scopes define the resources in your system
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            var user = new IdentityResource(
                "user",
                "Employee Details",
                new[] {
                    "employeeId",
                    "name",
                    "phoneNumber",
                    "companyId",
                    "companyName",
                    "role"
                }
            );
            return new List<IdentityResource> {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                user
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource> {
                new ApiResource(
                    "api1",
                    "otrack API",
                    new[] {"employeeId", "name", "phoneNumber", "companyId", "companyName", "role"}
                )
            };
        }

        // clients want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients()
        {
            // client credentials
            return new List<Client> {
                // implicit flow client (Restaurants SPA)
                new Client {
                    ClientId = "restaurants-app",
                    ClientName = "Restaurants App",
                    AllowedGrantTypes = GrantTypes.Implicit,

                    RedirectUris = GenerateRedirectUris(),
                    PostLogoutRedirectUris = GenerateLogoutUris(),
                    AccessTokenLifetime = 120, // just to test

                    RequireConsent = false, // no need for consent page
                    AllowOfflineAccess = true, // needed for silent renew
                    AllowAccessTokensViaBrowser = true, // needed because is a js app

                    AllowedScopes = {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "user",
                        "api1"
                    }
                },
                new Client {
                    ClientId = "distributor-app",
                    ClientName = "Android app client",
                    RequireClientSecret = false,
                    AllowedGrantTypes = GrantTypes.Code,
                    RequirePkce = true,
                    //AllowAccessTokensViaBrowser = true,
                    RequireConsent = false,

                    RedirectUris = {"info.otrack.distributorapp:/oauth2callback"},
                    PostLogoutRedirectUris = {"info.otrack.distributorapp.logout:/oauth2callback"},
                    AllowedScopes = {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        IdentityServerConstants.StandardScopes.Phone,
                        "user",
                        "api1"
                    },
                    AllowOfflineAccess = true
                }
            };
        }

        /*
        |-----------------------------------------------------------------------
        | URIS
        |-----------------------------------------------------------------------
        */
        // Allowed uris
        public static ICollection<string> GetAllowedUris()
        {
            return new List<string> {
                "http://localhost:3000",
                "https://staging.otrack.info",
                "https://otrack.info"
            };
        }

        private static List<string> GenerateLogoutUris()
        {
            return GetAllowedUris()
                .Select(url => url + "/auth/signout-oidc")
                .ToList();
        }

        private static List<string> GenerateRedirectUris()
        {
            var resultList = new List<string>();

            GetAllowedUris().ToList().ForEach(url =>
            {
                resultList.Add(url + "/auth/signin-oidc");
                resultList.Add(url + "/auth/signout-oidc");
                resultList.Add(url + "/auth/silent-renew-oidc");
            });

            return resultList;
        }
    }
}
