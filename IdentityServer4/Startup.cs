﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using IdentityServer4;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Entities;
using IdentityServer4.EntityFramework.Mappers;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using oTrackIdentityServer.Data.Repositories;
using oTrackIdentityServer.Options;

namespace oTrackIdentityServer {

    public class Startup {
        public Startup(IHostingEnvironment env) {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }
        private string otrackPolicy = "oTrackPolicy";

        public void ConfigureServices(IServiceCollection services) {
            services.AddMvc();

            services.AddCors(options => {
                options.AddPolicy(otrackPolicy,
                    builder => builder
                        .WithOrigins(Config.GetAllowedUris().ToArray())
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                );
            });

            // Configure the api url to use in repositories
            services.Configure<UserApiOptions>(Configuration);

            var connectionString = GetConnectionString();
            var pfxPassword = Configuration["otrackIdentityServer:PfxPassword"];
            var pfx = new X509Certificate2("otrackidserver.pfx", pfxPassword);

            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            services.AddIdentityServer()
                .AddSigningCredential(pfx)
                .AddConfigurationStore(builder =>
                    builder.UseNpgsql(connectionString, options =>
                        options.MigrationsAssembly(migrationsAssembly)))
                .AddOperationalStore(builder =>
                    builder.UseNpgsql(connectionString, options =>
                        options.MigrationsAssembly(migrationsAssembly)));

            services.AddScoped<EmployeeRepository>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
            ILoggerFactory loggerFactory) {
            InitializeDatabase(app);

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            // Because we have a nginx loadbalancer in the front, we need to pass
            // the right protocol so the discovery endpoint links can be https
            app.UseForwardedHeaders(new ForwardedHeadersOptions {
                ForwardedHeaders = ForwardedHeaders.XForwardedHost | ForwardedHeaders.XForwardedProto
            });

            app.UseCors(otrackPolicy);

            app.UseIdentityServer();

            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
        }

        /*
        |-----------------------------------------------------------------------
        | Seed the Database
        |-----------------------------------------------------------------------
        */
        private void InitializeDatabase(IApplicationBuilder app) {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope()) {
                // serviceScope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();

                var context = serviceScope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();
                //context.Database.Migrate();

                //bool seed = true;
                if (true) {
                    
                    context.Set<Client>().RemoveRange(context.Clients.ToList());

                    context.Set<Client>()
                        .AddRange(Config.GetClients().Select(client => client.ToEntity()));

                    context.SaveChanges();
                

                    context.Set<IdentityResource>().RemoveRange(context.IdentityResources.ToList());

                    context.Set<IdentityResource>()
                        .AddRange(Config.GetIdentityResources().Select(resource => resource.ToEntity()));

                    context.SaveChanges();

                
                    context.Set<ApiResource>().RemoveRange(context.ApiResources.ToList());

                    context.Set<ApiResource>()
                        .AddRange(Config.GetApiResources().Select(resource => resource.ToEntity()));

                    context.SaveChanges();
                    
                }
            }
        }

        /*
        |-----------------------------------------------------------------------
        | Connection String
        |-----------------------------------------------------------------------
        */
        private string GetConnectionString() {
            const string con = "oTrackIdentity:Database:PostgresConnection";
            if (!string.IsNullOrEmpty(Configuration[con])) {
                return Configuration[con];
            }

            var host = Getenv("DB_HOST");
            var name = Getenv("DB_NAME");
            var user = Getenv("DB_USER");
            var password = Getenv("DB_PASSWORD");

            return
                $"User ID={user};Password={password};Host={host};Port={5432};Database={name}";
        }

        private static string Getenv(string key) => Environment.GetEnvironmentVariable(key);
    }

}
