﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using oTrackIdentityServer.Models;
using oTrackIdentityServer.Options;

namespace oTrackIdentityServer.Data.Repositories {

    public class EmployeeRepository {
        
        private readonly string _url;

        public EmployeeRepository(IOptions<UserApiOptions> optionsAccessor) {
            _url = optionsAccessor.Value.API_URL;
        }

        public async Task<Employee> GetEmployeeByPhoneAndPassword(string phone, string password) {
            var data = new {
                PhoneNumber = phone,
                Password = password
            };
            using (var httpClient = new HttpClient())
            {
                var res = await httpClient.PostAsync(
                    _url + "/auth/login",
                    new StringContent(
                        JsonConvert.SerializeObject(data),
                        Encoding.UTF8, 
                        "application/json"
                    )
                );
                
                if (res.IsSuccessStatusCode) {
                    return JsonConvert.DeserializeObject<Employee>(await res.Content.ReadAsStringAsync());
                }
                
                return null;
            }
        }
    }

}
