## oTrack Authorization Server

Security Token Service used by all apps. 

Uses IdentityServer4 with the configuration stored in a BD.

### Requirements
- dotnet core runtime [download](https://www.microsoft.com/net/download/core)
- Postgres

### Database Connection
Add an environment variable with the name:

- Windows:
  - `oTrackIdentity:Database:PostgresConnection`
  - `otrackIdentityServer:PfxPassword`
- Unix based: (: char is not allowed)
  - `oTrackIdentity__Database__PostgresConnection`
  - `otrackIdentityServer__PfxPassword` 

Connection String Value: `User ID=username;Password=password;Host=localhost;Port=5432;Database=otrack`

### Instructions
- `cd IdentityServer4`
- `dotnet ef database update --context ConfigurationDbContext`
- `dotnet ef database update --context PersistedGrantDbContext`
- `dotnet run`


### Stack
- .NET Core 1.1 (MVC)
- IdentityServer4
- EF Core 1.1
- Postgres DB