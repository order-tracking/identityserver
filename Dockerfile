FROM microsoft/aspnetcore:1.1
COPY /IdentityServer4/bin/Release/PublishOutput/ /app
ENV ASPNETCORE_ENVIRONMENT=Production
ENV ASPNETCORE_URLS http://*:5000
WORKDIR /app
EXPOSE 5000
CMD ["dotnet","oTrackIdentityServer.dll"]
